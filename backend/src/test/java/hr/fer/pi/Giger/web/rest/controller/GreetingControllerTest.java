package hr.fer.pi.Giger.web.rest.controller;

import hr.fer.pi.Giger.BaseIntegrationTest;
import hr.fer.pi.Giger.domain.Greet;
import hr.fer.pi.Giger.repository.GreetingRepository;
import hr.fer.pi.Giger.web.rest.dto.GreetingDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;


class GreetingControllerTest extends BaseIntegrationTest {

    private static final String API_LINK_GREETING = "/greeting";

    @Autowired
    private WebApplicationContext context;
    @Autowired
    private GreetingRepository greetingRepository;

    private MockMvc mockMvc;

    @BeforeEach
    void setUpFixture() {
        mockMvc = webAppContextSetup(context).build();
    }

    @Test
    public void getGreet_GreetExists() throws Exception {
        var greet = greetingRepository.save(new Greet(null, "title", "content"));

        var response = mockMvc.perform(
                get(API_LINK_GREETING + "/" + greet.getId()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        GreetingDto greetingDto = objectMapper.readValue(response.getContentAsString(), GreetingDto.class);

        Assertions.assertEquals("title", greetingDto.getTitle());
        Assertions.assertEquals("content", greetingDto.getContent());
    }
}