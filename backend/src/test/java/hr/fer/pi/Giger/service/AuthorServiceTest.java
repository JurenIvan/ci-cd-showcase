package hr.fer.pi.Giger.service;

import hr.fer.pi.Giger.domain.Author;
import hr.fer.pi.Giger.repository.AuthorRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AuthorServiceTest {

    @InjectMocks
    private AuthorService authorService;

    @Mock
    private AuthorRepository authorRepository;

    @Mock
    private Author author;

    @Test
    void findAllGreetings_AuthorExists_ReturnGreetings() {
        when(authorRepository.findById(1L)).thenReturn(Optional.of(author));

        authorService.findAllGreetings(1L);

        verify(author).getGreetList();
    }

    @Test
    void findAllGreetings_AuthorDoesntExist_throwException() {
        when(authorRepository.findById(1L)).thenReturn(Optional.empty());

        Assertions.assertThrows(IllegalArgumentException.class, () -> authorService.findAllGreetings(1L));
    }

    @Test
    void findAuthorData_AuthorExists_ReturnGreetings() {
        when(authorRepository.findById(1L)).thenReturn(Optional.of(author));

        var result = authorService.findAuthorData(1L);

        Assertions.assertSame(author, result);
    }

    @Test
    void findAuthorData_AuthorDoesntExist_throwException() {
        when(authorRepository.findById(1L)).thenReturn(Optional.empty());

        Assertions.assertThrows(IllegalArgumentException.class, () -> authorService.findAuthorData(1L));
    }
}