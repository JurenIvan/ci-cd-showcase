package hr.fer.pi.Giger;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class GigerApplicationTests {

    @Test
    void contextLoads() {
        Assertions.assertEquals(5, 2 + 3);
    }
}
