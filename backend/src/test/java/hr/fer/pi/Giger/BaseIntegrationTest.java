package hr.fer.pi.Giger;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class BaseIntegrationTest {

    @Autowired
    protected ObjectMapper objectMapper;

    @BeforeAll
    static void test() {
        Assertions.assertDoesNotThrow(() -> {
        });
    }
}
