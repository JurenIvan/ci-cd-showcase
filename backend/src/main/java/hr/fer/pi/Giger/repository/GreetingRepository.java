package hr.fer.pi.Giger.repository;

import hr.fer.pi.Giger.domain.Greet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GreetingRepository extends JpaRepository<Greet, Long> {
}
