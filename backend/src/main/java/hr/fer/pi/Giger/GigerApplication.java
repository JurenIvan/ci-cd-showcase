package hr.fer.pi.Giger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GigerApplication {

	public static void main(String[] args) {
		SpringApplication.run(GigerApplication.class, args);
	}

}
