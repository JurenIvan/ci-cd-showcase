package hr.fer.pi.Giger.web.rest.dto;

import hr.fer.pi.Giger.domain.Greet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GreetingDto {

    private Long id;
    private String title;
    private String content;

    public static GreetingDto fromEntity(Greet entity) {
        return new GreetingDto(entity.getId(), entity.getTitle(), entity.getContent());
    }
}
