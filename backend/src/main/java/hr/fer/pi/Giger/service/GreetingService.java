package hr.fer.pi.Giger.service;

import hr.fer.pi.Giger.domain.Greet;
import hr.fer.pi.Giger.repository.GreetingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GreetingService {

    private final GreetingRepository greetingRepository;

    public Greet fetchGreet(Long greetId) {

        var optionalGreet = greetingRepository.findById(greetId);
        if (optionalGreet.isEmpty())
            throw new IllegalArgumentException("No such greet");

        return optionalGreet.get();
    }
}

