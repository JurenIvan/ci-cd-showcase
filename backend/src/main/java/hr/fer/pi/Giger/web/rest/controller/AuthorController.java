package hr.fer.pi.Giger.web.rest.controller;

import hr.fer.pi.Giger.service.AuthorService;
import hr.fer.pi.Giger.web.rest.dto.AuthorDto;
import hr.fer.pi.Giger.web.rest.dto.GreetingDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/authors")
@RequiredArgsConstructor
public class AuthorController {

    private final AuthorService authorService;

    @GetMapping("/greetings/{id}")
    public List<GreetingDto> findGreetingsFromAuthor(@PathVariable Long id) {
        return authorService.findAllGreetings(id).stream().map(GreetingDto::fromEntity).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public AuthorDto fetchAuthorData(@PathVariable Long id) {
        return AuthorDto.fromEntity(authorService.findAuthorData(id));
    }
}
