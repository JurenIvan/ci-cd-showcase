package hr.fer.pi.Giger.web.rest.controller;

import hr.fer.pi.Giger.service.GreetingService;
import hr.fer.pi.Giger.web.rest.dto.GreetingDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/greeting")
@RequiredArgsConstructor
public class GreetingController {

    private final GreetingService greetingService;

    @GetMapping("/{id}")
    public GreetingDto findGreet(@PathVariable Long id) {
        return GreetingDto.fromEntity(greetingService.fetchGreet(id));
    }
}
