package hr.fer.pi.Giger.service;

import hr.fer.pi.Giger.domain.Author;
import hr.fer.pi.Giger.domain.Greet;
import hr.fer.pi.Giger.repository.AuthorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthorService {

    private final AuthorRepository authorRepository;

    public List<Greet> findAllGreetings(Long authorId) {
        var optionalAuthor = authorRepository.findById(authorId);
        if (optionalAuthor.isEmpty())
            throw new IllegalArgumentException("No such author");

        return optionalAuthor.get().getGreetList();
    }

    public Author findAuthorData(Long authorId) {
        var optionalAuthor = authorRepository.findById(authorId);
        if (optionalAuthor.isEmpty())
            throw new IllegalArgumentException("No such author");

        return optionalAuthor.get();
    }
}

