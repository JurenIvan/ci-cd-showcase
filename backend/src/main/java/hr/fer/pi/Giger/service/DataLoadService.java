package hr.fer.pi.Giger.service;

import hr.fer.pi.Giger.domain.Author;
import hr.fer.pi.Giger.domain.Greet;
import hr.fer.pi.Giger.repository.AuthorRepository;
import hr.fer.pi.Giger.repository.GreetingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DataLoadService implements ApplicationRunner {

    private final GreetingRepository greetingRepository;
    private final AuthorRepository authorRepository;

    private List<Author> authors = new ArrayList<>();
    private List<Greet> greetings = new ArrayList<>();

    @Override
    public void run(ApplicationArguments args) throws Exception {
        createAuthors();
        createGreetings();
        linkGreetingsToAuthors();
    }

    private void createAuthors() {
        authors.add(authorRepository.save(new Author(null, "John", "John.doe@gmail.com", List.of())));
        authors.add(authorRepository.save(new Author(null, "Marie", "Marie.doe@gmail.com", List.of())));
        authors.add(authorRepository.save(new Author(null, "Elvis", "Elvis.doe@gmail.com", List.of())));
        authors.add(authorRepository.save(new Author(null, "Emma", "Emma.doe@gmail.com", List.of())));
    }

    private void createGreetings() {
        greetings.add(greetingRepository.save(new Greet(null, "Hello", "Hello, this is not usefull content")));
        greetings.add(greetingRepository.save(new Greet(null, "Hi", "Hi, This is usefull content")));
        greetings.add(greetingRepository.save(new Greet(null, "Ciaoo", "Ciaoo pricipessa")));
        greetings.add(greetingRepository.save(new Greet(null, "Title", "Title of the news ")));
        greetings.add(greetingRepository.save(new Greet(null, "News", "Epsin didn't kill himself")));
    }

    private void linkGreetingsToAuthors() {
        authors.get(0).setGreetList(List.of(greetings.get(0), greetings.get(1), greetings.get(2)));
        authors.get(1).setGreetList(List.of(greetings.get(4), greetings.get(3)));
        authorRepository.saveAll(authors);
    }
}

