package hr.fer.pi.Giger.web.rest.dto;

import hr.fer.pi.Giger.domain.Author;
import hr.fer.pi.Giger.domain.Greet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import static java.util.stream.Collectors.toList;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AuthorDto {

    private Long id;
    private String name;
    private String email;
    private List<Long> greetingsIds;

    public static AuthorDto fromEntity(Author entity) {
        return new AuthorDto(entity.getId(), entity.getName(), entity.getEmail(), entity.getGreetList().stream().map(Greet::getId).collect(toList()));
    }
}
